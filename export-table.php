<?php
namespace Amaia_showroom\Helpers;
include('jp_library/jp_lib.php');
include("w_services/dashboard_functions.php");

// get Data -- start
if(isset($_GET['start_date']))
{
  $registrations = getRegistrations(false, "", $_GET['start_date'], $_GET['end_date'], $_GET['showroom']);
}
else
{
  $registrations = getRegistrations(false, "", "", "", "", false, true);
}

// $all_data_showrooms = getAllShowrooms(false, $data);
$all_data_showrooms = getAllProjects(false, $data);
$all_data_source = getAllData(false, $data, 'source');
$all_data_purpose = getAllData(false, $data, 'purpose');
$all_data_age = getAllData(false, $data, 'age');
$all_data_civil_status = getAllData(false, $data, 'civil_status');
// get Data -- end

$columns = [
  "Name",
  "Seller Name",
  "Date Registered",
  "Contact No.",
  "Email",
  "Buying Purpose",
  "Project",
  "Find out Amaia by",
  "Walk In",
  "Guests No.",
  "Age",
  "Gender",
  "Civil Status"
];
$csv = new CSV($columns);
$all_properties_export = "";


foreach ($registrations as $registration_id => $arr_registration) { 

  // get properties - start
  foreach ($arr_registration['showrooms'] as $arr_showrooms) {
    $exploded_showrooms = explode(",", $arr_showrooms['properties']);
    $all_properties_export = "";
    foreach ($exploded_showrooms as $clean_showroom) {
      $sanitized_showroom = str_replace("*", "", $clean_showroom);
      $all_properties_export .= $all_data_showrooms[$sanitized_showroom]['title'];  
    }                                         
  }
  // get properties - end

  if(isset($arr_registration['showrooms']))
  {
    $csv_arr = array(
      $arr_registration['f_name']." ".$arr_registration['l_name'],
      $arr_registration['seller_name'],
      $arr_registration['date_registered'],
      $arr_registration['contact_num'],
      $arr_registration['e_mail'],
      $all_data_purpose[$arr_registration['buying_purpose']],
      $all_properties_export,
      $all_data_source[$arr_registration['find_out_amaia']],
      $arr_registration['is_walk_in'] == 1 ? "Yes" : "No",
      $arr_registration['guests_num'],
      $all_data_age[$arr_registration['age']],
      $arr_registration['gender'] < 2 ? ($arr_registration['gender'] == 1 ? "Male" : "Female") : "Others",
      $all_data_civil_status[$arr_registration['civil_status']]
    );
    $csv->addRow($csv_arr);
  }
} 

// while ($row = mysqli_fetch_array($result)) {

  

// }

$csv->export();

exit;
