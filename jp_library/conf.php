<?php
$LOCAL_MODE = true; //SET TO TRUE ONLY IN LOCAL COMPUTER




require __DIR__ . "/../helpers/CSV.php";
use Amaia_showroom\Helpers\CSV;

#MODULE CONTROLS
$AVATAR = true;
$LEFT_SIDEBAR = true;
$RIGHT_SIDEBAR = false;
$SEARCH = false;
$LANGUAGE = true;
$NOTIFICATION = true;
$LOGO = true;

#DATETIME SETTINGS
date_default_timezone_set('Asia/Manila');

#DEFAULT MODULE INITIALIZATIONS
#OVERRIDE ON YOUR PAGE IF NECESSARY
$DYNAMIC_TABLE = false;
$PICKERS = false; #FOR TIMEPICKERS, DATEPICKERS, ETC

#SITE CONFIG
#$SITE_NAME = '临床研究管理系统';
// $SITE_NAME = '科群迈谱 [CTRIMAP]';
// $LOGO_NAME = '<b>科群迈谱</b>[CTRIMAP] <span style="font-size:20px">临床试验研究管理平台</span>';

#URL CONTROLS
$PAGE_NAME = basename($_SERVER['PHP_SELF']);

if ($LOCAL_MODE){
    $BASE_URL = "http://localhost/amaia_showroom/";
    $GLOBALS['base_url'] = "http://localhost/amaia_showroom/";
}
else{
    $BASE_URL = "http://betaprojex.com/amaia_showroom/";
    $GLOBALS['base_url'] = "http://betaprojex.com/amaia_showroom/";
}



