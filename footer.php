<!--footer start-->
<footer class="site-footer">
  <div class="text-center">
      2013 &copy; FlatLab by VectorLab.
      <a href="#" class="go-top">
          <i class="fa fa-angle-up"></i>
      </a>
  </div>
</footer>
<!--footer end-->
<script>
function ajax_load(form_data, file_url, element_id)
{
    var final_url = "ajax-load/"+file_url+"?";

    if(!form_data){
//       #if form data is empty?!!?!
    }else{
        $.each(form_data, function( key, value ){
            final_url += key+"="+value+"&";
        });
    }

	var xmlhttp;
	if (window.XMLHttpRequest){
	  // code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}else{
	  // code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
	  if (xmlhttp.readyState==4 && xmlhttp.status==200){
	    document.getElementById(element_id).innerHTML=xmlhttp.responseText;
//      CALLBACKS
        if(element_id == 'timeline_row')
        {
          $('#timeline_row').fadeIn(400);
          //$("html, body").animate({ scrollTop: $(document).height() }, 1000);
          //$("html, body").animate({ scrollTop: $("#timeline_row").scrollTop() }, 1000);
          $("html, body").animate({ scrollTop: 750 }, 1000);
        }

       if(element_id == 'cmt_body'){
           cmt_count = $("#cmt_body .cmt_row").length;

           if(cmt_count > 0){
               $("#status_submit").prop('disabled', false);
           }
       }
        if(element_id == 'i_body'){
           i_count = $("#i_body .i_row").length;

           if(i_count > 0){
               $("#status_submit").prop('disabled', false);
           }
       }

       if(element_id == 'multi_select_div'){
          $('#my_multi_select3').multiSelect({
          selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='搜索'>",
          selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='搜索'>",
          afterInit: function (ms) {
              var that = this,
                  $selectableSearch = that.$selectableUl.prev(),
                  $selectionSearch = that.$selectionUl.prev(),
                  selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                  selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

              that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                  .on('keydown', function (e) {
                      if (e.which === 40) {
                          that.$selectableUl.focus();
                          return false;
                      }
                  });

              that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                  .on('keydown', function (e) {
                      if (e.which == 40) {
                          that.$selectionUl.focus();
                          return false;
                      }
                  });
          },
          afterSelect: function () {
              this.qs1.cache();
              this.qs2.cache();
          },
          afterDeselect: function () {
              this.qs1.cache();
              this.qs2.cache();
          }
      });
       }
	  }
	}
	xmlhttp.open("GET",final_url,true);
	xmlhttp.send();
}
</script>