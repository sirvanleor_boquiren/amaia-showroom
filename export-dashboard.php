<?php
namespace Amaia_showroom\Helpers;
include('jp_library/jp_lib.php');
include("w_services/dashboard_functions.php"); # $data contains all json
$final_data = array();
$final_data['columns'][0] = "Date of Visit";

foreach ($data['locations'] as $loc_key => $loc_value) {
	foreach ($loc_value['properties'] as $pro_key => $pro_value) {
		$single_showroom = null;
		if(isset($_GET['d_showroom']) && $_GET['d_showroom'] != '')
		{
			$single_showroom = $_GET['d_showroom'];
		}
		if(isset($_GET['m_showroom']) && $_GET['m_showroom'] != '')
		{
			$single_showroom = $_GET['m_showroom'];	
		}

		if($single_showroom != null)
		{
			if($pro_value["id"] == $single_showroom)
			{
				$prop_id[$pro_value["id"]] = $pro_value["title"];
				$final_data['columns'][$pro_value['id']] = $pro_value["title"];				
			}
		}
		else
		{
			$prop_id[$pro_value["id"]] = $pro_value["title"];
			$final_data['columns'][$pro_value['id']] = $pro_value["title"];
		}
	}	
}

if(isset($_GET['type']) && $_GET['type'] == "m")
{
	$start_year = date("Y");		# DEFAULT: Year of today
	$start_month = "01";			# DEFAULT: January first month
	$end_year = date("Y");			# DEFAULT: Year Today
	$end_month = date("m");			# DEFAULT: Month today

	$months_diff = "-12"; 			# DEFAULT: Date difference is from month today and last 12 months
	$changed = 0;


	if(isset($_GET['m_start_month']) && $_GET['m_start_month'] != '')
	{
		$start_month = $_GET['m_start_month'];
		if($start_month < 10)
		{
			$start_month = "0".$start_month;
		}
		$changed = 1;
	}

	if(isset($_GET['m_end_month']) && $_GET['m_end_month'] != '')
	{
		$end_month = $_GET['m_end_month'];
		if($end_month < 10)
		{
			$end_month = "0".$end_month;
		}
	}

	if(isset($_GET['m_start_year']) && $_GET['m_start_year'] !='')
	{
		$start_year = $_GET["m_start_year"];
		$changed = 1; 			# Start Year Value Set
	}
	
	if(isset($_GET['m_end_year']) && $_GET['m_end_year'] !='')
	{
		$end_year = $_GET['m_end_year'];
	}

	$end_date = $end_year."-".$end_month."-".date("d");
	if($changed == 0)
	{
		$start_date = date("Y-m-d", strtotime("-12 months", strtotime($end_date)));
		$start_month = date("m", strtotime($start_date));
		$start_year = date("Y", strtotime($start_date));
	}
	else
	{
		$start_date = $start_year."-".$start_month."-".date("d");
	}

	$final_data['error_msg'] = "End Date should be GREATER than Start Date";
	if($end_year >= $start_year)
	{
		$final_data['error_msg'] = "";
		if($end_month >= $start_month)
		{
			$final_data['error_msg'] = "";
			$date_first = new \DateTime($end_date);
			$date_second = new \DateTime($start_date);
			$date_diff = $date_second->diff($date_first);
			$years = $date_diff->format("%y");
			$months_diff = $date_diff->format("%m");
			if($years > 0)
			{
				$months_diff += $years * 12;
			}
			// echo $months_diff;
			$ctr_date = $start_date;
			$final_data['row_data'] = array();
			for($ctr=0;$ctr<=$months_diff;$ctr++) {
				$final_data['row_data'][$ctr_date] = array();
				$final_data['row_data'][$ctr_date][] = date("Y", strtotime($ctr_date))."-".date("m", strtotime($ctr_date));
				$ctr_month = date("m", strtotime($ctr_date));
				$ctr_year = date("Y", strtotime($ctr_date));
				foreach ($prop_id as $prop_arr_id => $prop_title) {
					$count_data['select'] = "COUNT(location_id) as total";
					$count_data['table'] = "location_interested";
					$count_data['where'] = "properties = '$prop_arr_id' AND extract(month from date_registered) = $ctr_month AND extract(year from date_registered) = $ctr_year";
					$res_data = jp_get($count_data);
					$row_data = mysqli_fetch_assoc($res_data);
					$final_data['row_data'][$ctr_date][$prop_arr_id] = $row_data['total'];
				}
				$ctr_date = date("Y-m-d", strtotime("+1 month", strtotime($ctr_date)));
			}
		}
	}
}
else
{
	$month = date("m", strtotime(date("Y-m-d")));
	$year = date("Y", strtotime(date("Y-m-d")));
	
	if(isset($_GET['d_year']) && $_GET['d_year'] != '')
	{
		$year = $_GET['d_year'];
	}
	if(isset($_GET['d_month']) && $_GET['d_month'] != '')
	{
		$month = $_GET['d_month'];
	}
	$date = $year."-".$month."-01";
	$final_date = date("t", strtotime($date));
	$final_data['row_data'] = array();

	for ($start_day=1; $start_day < $final_date; $start_day++) { 
		$final_data['row_data'][$date] = array();
		$final_data['row_data'][$date][] = $date;
		foreach ($prop_id as $prop_arr_id => $prop_title) {
			$count_data['select'] = "COUNT(location_id) as total";
			$count_data['table'] = "location_interested";
			$count_data['where'] = "properties = '$prop_arr_id' AND date_registered = '$date'";
			$res_data = jp_get($count_data);
			$row_data = mysqli_fetch_assoc($res_data);
			$final_data['row_data'][$date][$prop_arr_id] = $row_data['total'];
		}
		$date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
	}	
}

$csv_columns = $final_data['columns'];
$csv = new CSV($csv_columns);
foreach ($final_data['row_data'] as $date_of_visit => $csv_row) {
	$csv->addRow($csv_row);
}
$csv->export();
 // var_dump($final_data);
?>
