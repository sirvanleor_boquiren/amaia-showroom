  <?php include("jp_library/jp_lib.php"); ?>
  <?php include("head.php"); ?>
  <?php include("w_services/dashboard_functions.php"); 
        $all_data_showrooms = getAllShowrooms(false, $data);
        $graph_color_arr = ['#75bb95','#ffa700','#d6d8a0','#033658','#a4d419','#070ce7','#f050fe','#f84475','#08047e',
                  '#b38519','#530006','#008b45','#6c1a20','#4f0409','#e59ca6','#d2a8a9','#8cf0db','#f47676',
                  '#f29c54','#ead13c','#a9496e','#d2a8a9','#b7d2a8','#e7d2a8','#3f4145','#0470b1','#00ffff']; # sequential based on data.json

  ?>
  <body>
  <section id="container" class="">
      <?php include("header.php"); ?>
      <?php include("sidebar.php"); ?>
      <!--main content start-->
      <section id="main-content" >
          <section class="wrapper site-min-height">
            <!-- page start-->
            <!-- Graph start -->
            <div id="morris">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                        <?php 
                        $month_text = date("F", strtotime(date("Y-m-d"))); 
                        $year_text = date("Y", strtotime(date("Y-m-d")));
                        if(isset($_GET['d_month']) && $_GET['d_month'] != '')
                        {
                          $month_text = date("F", strtotime(date("Y-".$_GET['d_month']."-d")));
                        }
                        if(isset($_GET['d_year']) && $_GET['d_year'] != '')
                        {
                          $year_text = $_GET['d_year'];
                        }
                        $date_text = $month_text." ".$year_text;
                        ?>
                            No. of Visits per Showroom in the month of <?php echo $date_text; ?>
                        </header>
                        <div class="panel-body" style="height:800px">
                          <!-- Table Filters start -->
                          <form method="GET" name="reg_filters" id="reg_filters">
                            <div class="row">
                              <div class="col-lg-12">
                                  <div class="form-group">

                                    <!-- <label class="control-label col-md-1">Report Type</label> -->
                                    <div class="col-md-2">
                                      <select class="form-control" name="type" id="type" onchange="change_type(this.value);">
                                        <option value="">Report Type</option>
                                        <option value="d" <?php echo isset($_GET['type']) && $_GET['type'] == "d" ? "selected" : ""; ?>>Daily</option>
                                        <option value="m" <?php echo isset($_GET['type']) && $_GET['type'] == "m" ? "selected" : ""; ?>>Monthly</option>
                                      </select>
                                    </div>


                                    <div id="daily_filters" style="display:<?php echo isset($_GET['type']) && $_GET['type'] == "d" ? "block":"none"; ?>">
                                      <!-- <label class="control-label col-md-1">Year</label> -->
                                      <div class="col-md-2">
                                        <select class="form-control" name="d_year" id="d_year">
                                          <option value="">Year</option>
                                          <?php 
                                          $curr_date = date("Y", strtotime(date("Y-m-d"))); # select current month
                                          $year_ctr = $curr_date;
                                          for ($i=0; $i <= 20; ++$i) { ?>
                                            <option value="<?php echo $year_ctr; ?>" <?php echo isset($_GET['d_year']) && $_GET['d_year'] == $year_ctr ? "selected":""; ?>><?php echo $year_ctr; ?></option>
                                          <?php $year_ctr--; } ?>
                                        </select>
                                      </div>
                                      <!-- <label class="control-label col-md-1">Month</label> -->
                                      <div class="col-md-2">
                                        <select class="form-control" name="d_month" id="d_month">
                                          <option value="">Month</option>
                                          <?php 
                                          $curr_date = date("m", strtotime(date("Y-m-d"))); # select current month
                                          if(isset($_GET['d_month']))
                                          {
                                            $curr_date = date("m", strtotime(date("Y-".$_GET['d_month']."-d")));
                                          }
                                          for ($i=1; $i < 13; ++$i) { ?>
                                            <option value="<?php echo $i < 10 ? "0".$i:$i; ?>" <?php echo $curr_date == $i ? "selected":""; ?>><?php echo date("F", strtotime("+".$i." month", strtotime("2016-12-01"))) ?></option>
                                          <?php } ?>
                                        </select>
                                      </div>
                                      <!-- <label class="control-label col-md-1">Showroom</label> -->
                                     <!--  <div class="col-md-3">
                                          <select class="form-control" name="d_showroom" id="d_showroom">
                                            <option value="">Specific Showroom</option>
                                          <?php foreach ($all_data_showrooms as $showroom_id => $arr_value) { ?>
                                            <option value="<?php echo $showroom_id; ?>" <?php echo isset($_GET['d_showroom']) && $_GET['d_showroom'] == $showroom_id ? "selected" : ""; ?>><?php echo $arr_value['title']; ?></option>
                                          <?php } ?>
                                        </select>
                                      </div> -->
                                    </div>

                                    <div id="monthly_filters" style="display:<?php echo isset($_GET['type']) && $_GET['type'] == "m" ? "block":"none"; ?>">
                                      <!-- <label class="control-label col-md-1">Year</label> -->
                                      <div class="col-md-1">
                                        <select class="form-control" name="m_start_year" id="m_start_year" style="width:90px">
                                          <option value="">Year</option>
                                          <?php 
                                          $curr_date = date("Y", strtotime(date("Y-m-d"))); # select current month
                                          $year_ctr = $curr_date;
                                          for ($i=0; $i <= 20; ++$i) { ?>
                                            <option value="<?php echo $year_ctr; ?>" <?php echo isset($_GET['m_start_year']) && $_GET['m_start_year'] == $year_ctr ? "selected" : ""; ?>><?php echo $year_ctr; ?></option>
                                          <?php $year_ctr--; } ?>
                                        </select>
                                      </div>
                                      <!-- <label class="control-label col-md-1">Month</label> -->
                                      <div class="col-md-2">
                                        <select class="form-control" name="m_start_month" id="m_start_month" style="margin-left:20px">
                                          <option value="">Month</option>
                                          <?php 
                                          $curr_date = date("m", strtotime(date("Y-m-d"))); # select current month
                                          for ($i=1; $i < 13; ++$i) { ?>
                                            <option value="<?php echo $i; ?>" <?php echo isset($_GET['m_start_month']) && $_GET['m_start_month'] == $i ? "selected" : ""; ?>><?php echo date("F", strtotime("+".$i." month", strtotime("2016-12-01"))) ?></option>
                                          <?php } ?>
                                        </select>
                                      </div>
                                      <label class="col-md-1" style="width: 10px;">To</label>
                                      <div class="col-md-1">
                                        <select class="form-control" name="m_end_year" id="m_end_year" style="width:90px">
                                          <option value="">Year</option>
                                          <?php 
                                          $curr_date = date("Y", strtotime(date("Y-m-d"))); # select current month
                                          $year_ctr = $curr_date;
                                          for ($i=0; $i <= 20; ++$i) { ?>
                                            <option value="<?php echo $year_ctr; ?>" <?php echo isset($_GET['m_end_year']) && $_GET['m_end_year'] == $year_ctr ? "selected" : ""; ?>><?php echo $year_ctr; ?></option>
                                          <?php $year_ctr--; } ?>
                                        </select>
                                      </div>
                                      <!-- <label class="control-label col-md-1">Month</label> -->
                                      <div class="col-md-2">
                                        <select class="form-control" name="m_end_month" id="m_end_month" style="margin-left:20px">
                                          <option value="">Month</option>
                                          <?php 
                                          $curr_date = date("m", strtotime(date("Y-m-d"))); # select current month
                                          for ($i=1; $i < 13; ++$i) { ?>
                                            <option value="<?php echo $i; ?>" <?php echo isset($_GET['m_end_month']) && $_GET['m_end_month'] == $i ? "selected" : ""; ?>><?php echo date("F", strtotime("+".$i." month", strtotime("2016-12-01"))) ?></option>
                                          <?php } ?>
                                        </select>
                                      </div>
                                       <!-- <label class="control-label col-md-1">Showroom</label> -->
                                    </div>
                                        <div class="col-md-3">
                                            <select class="form-control" name="showroom" id="showroom" style="display:<?php echo isset($_GET['type']) ? 'block' : 'none'; ?>">
                                              <option value="">Specific Showroom</option>
                                            <?php foreach ($all_data_showrooms as $showroom_id => $arr_value) { ?>
                                              <option value="<?php echo $showroom_id; ?>" <?php echo isset($_GET['m_showroom']) && $_GET['m_showroom'] == $showroom_id ? "selected" : ""; ?>><?php echo $arr_value['title']; ?></option>
                                            <?php } ?>
                                          </select>
                                        </div>
                                  </div>
                              </div>
                            </div>
                            <br>
                            <div class="row">
                              <div class="col-md-4">
                                <div class="col-lg-12">
                                      <button class="btn btn-success" type="submit">Search</button>
                                      <a href="<?php echo $BASE_URL.$PAGE_NAME; ?>" class="btn btn-success">Clear</a>
                                      <?php
                                      $get_filters = "";
                                      if(isset($_GET))
                                      {
                                        $get_filters_arr = array();
                                        foreach ($_GET as $key => $value) {
                                          $get_filters_arr[] .= $key."=".$value;
                                        }
                                        $get_filters = "?".implode("&", $get_filters_arr);
                                      }
                                      ?>
                                      <a href="<?php echo $BASE_URL; ?>export-dashboard.php<?php echo $get_filters; ?>" class="btn btn-success">Export</a>
                                </div>
                              </div>
                            </div>
                          </form>
                          <form name="gFilters" id="gFilters" style="display:none">
                            <input name="year"value="<?php echo isset($_GET['d_year']) && $_GET['d_year'] != "" ? $_GET['d_year'] : ""; ?>">
                            <input name="month"value="<?php echo isset($_GET['d_month']) && $_GET['d_month'] != "" ? $_GET['d_month'] : ""; ?>">
                            <input id="start_month" name="start_month"value="<?php echo isset($_GET['m_start_month']) && $_GET['m_start_month'] != "" ? $_GET['m_start_month'] : ""; ?>">
                            <input id="start_year" name="start_year"value="<?php echo isset($_GET['m_start_year']) && $_GET['m_start_year'] != "" ? $_GET['m_start_year'] : ""; ?>">
                            <input id="end_month" name="end_month"value="<?php echo isset($_GET['m_end_month']) && $_GET['m_end_month'] != "" ? $_GET['m_end_month'] : ""; ?>">
                            <input id="end_year" name="end_year"value="<?php echo isset($_GET['m_end_year']) && $_GET['m_end_year'] != "" ? $_GET['m_end_year'] : ""; ?>">
                            <input name="showroom" value="<?php echo isset($_GET['showroom']) && $_GET['showroom'] != "" ? $_GET['showroom'] : ""; ?>">
                            <input name="type"value="<?php echo isset($_GET['type']) && $_GET['type'] != "" ? $_GET['type'] : ""; ?>">
                          </form>
                            <!-- Table Filters end -->
                            <br>
                            <h4 id="hey">Legend</h4>
                            <br>
                            <?php 
                            $color_ctr = "0";
                            $row_ctr = "1";
                            $end_of_arr = sizeof($all_data_showrooms);
                            foreach ($all_data_showrooms as $showroom_id => $arr_value) { 
                              if($row_ctr == 1)
                              {
                                echo "<div class='col-md-3'>";
                              }
                              echo "<div class='row' style='margin-bottom:4px;'>";
                              echo "  <div class='col-lg-1' style='background:".$graph_color_arr[$color_ctr]."; width:25px'>&nbsp;";
                              echo "  </div>";
                              echo "  <div class='col-lg-10'>";
                              echo "    ".$arr_value['title'];
                              echo "  </div>";
                              echo "</div>";
                              if($row_ctr == 10 || $color_ctr == $end_of_arr - 1)
                              {
                                echo "</div>";
                                $row_ctr = 0;
                              }
                              $row_ctr++;
                              $color_ctr++;
                            }

                            ?>
                            
                            <div id="hero-area" class="graph"></div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- Graph end -->
            <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <?php include("footer.php"); ?>
  </section>
    <?php include("scripts.php"); ?>
    <script>  
    var end_y = parseInt($('#end_year').val());
    var end_m = parseInt($('#end_month').val());
    var start_y = parseInt($('#start_year').val());
    var start_m = parseInt($('#start_month').val());
    var err = 1;
    <?php if(isset($_GET['type'])): ?>
    if(end_y > start_y)
    {
      err = 0;
    }
    else if(end_y == start_y)
    {
      if(end_m > start_m)
      {
        err = 0;
      }
    }

    if(err == 1)
    {
      if($('#start_year').val() != "" && $('#start_month').val() != "" && $('#end_year').val() != "" && $('#end_month').val() != "")
      alert("Warning: End Date should be GREATER than Start Date");
    }
    $.ajax({
      url:"w_services/get_db_data_area.php",
      type: "POST",
      dataType: "json",
      data: $('#gFilters').serialize(),
      success:function(data){
        console.log(data);
        if(data["msg"] != "")
        {
          var ykeys_val=[];
          for(var y in data["ykeys"]){
            ykeys_val.push(data["ykeys"][y]);
          }
          var labels=[];
          for(var l in data["labels"]){
            labels.push(data["labels"][l]);
          }
          var graph_data_arr=[];
          for(var g in data["graph_plot"]){
            graph_data_arr.push(data["graph_plot"][g]);
          }
          var colors_arr=[];
          for(var c in data["color"]){
            colors_arr.push(data["color"][c]);
          }
          // console.log(graph_data_arr);
          Morris.Line({
            element: 'hero-area',
            data: graph_data_arr,
            xkey: 'period',
            ykeys: ykeys_val,
            labels: labels,
            hideHover: 'auto',
            lineWidth: 1,
            pointSize: 5,
            lineColors: colors_arr,
            fillOpacity: 0.5,
            smooth: true
          });
        }
      },
      error:function(e)
      {
        console.log(e);
      }
    });
      
    <?php endif; ?>
      
      function change_type(value)
      {
        $('#showroom').prop("selectedIndex", 0);
        $('#showroom').attr("style", "display:block");
        if(value != "")
        {
          if(value == "d")
          {
            $('#daily_filters').attr("style", "display:block");
            $('#monthly_filters').attr("style", "display:none");
          }
          else
          {
            $('#daily_filters').attr("style", "display:none");
            $('#monthly_filters').attr("style", "display:block");
          }
        }
      }

      

      
    </script>
  </body>
</html>
