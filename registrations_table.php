                <?php //var_dump($all_data_showrooms); ?>
                <div class="row">
                  <div class="col-lg-12">
                    <section class="panel">
                      <header class="panel-heading">
                          Registrations
                      </header>
                      <div class="panel-body">
                        <!-- Table Filters start -->
                        <div class="row">
                          <div class="col-lg-10">
                            <form method="GET" name="reg_filters" id="reg_filters">
                              <div class="form-group">
                                <label class="control-label col-md-1">Date Range</label>
                                <div class="col-md-2">
                                  <input class="form-control form-control-inline input-medium default-date-picker"  name="start_date" id="start_date" size="16" type="text" value="<?php echo isset($_GET['start_date']) ? $_GET['start_date'] : ""; ?>" />
                                  <span class="help-block">Start date</span>
                                </div>
                                <div class="col-md-2">
                                  <input class="form-control form-control-inline input-medium default-date-picker"  name="end_date" id="end_date" size="16" type="text" value="<?php echo isset($_GET['end_date']) ? $_GET['end_date'] : ""; ?>" />
                                  <span class="help-block">End date</span>
                                </div>

                                <label class="control-label col-md-1">Showroom</label>
                                <div class="col-md-3">
                                    <select class="form-control" name="showroom" id="showroom">
                                      <option value="">Select showroom</option>
                                    <?php foreach ($all_data_showrooms_select as $showroom_id => $arr_value) { ?>
                                      <option value="<?php echo $showroom_id; ?>" <?php echo isset($_GET['showroom']) && $_GET['showroom'] == $showroom_id ? "selected" : ""; ?>><?php echo $arr_value['title']; ?></option>
                                    <?php } ?>
                                  </select>

                                </div>
                                <div class="col-md-3">
                                  <input type="hidden" name="p" id="p" value="<?php echo isset($_GET['p']) ? $_GET['p'] : ""; ?>">
                                  <button class="btn btn-success" type="submit">Search</button>
                                  <a href="<?php echo $BASE_URL; ?>registrations.php" class="btn btn-success">Clear</a>
                                  <?php
                                  $get_filters = "";
                                  if(isset($_GET))
                                  {
                                    $get_filters_arr = array();
                                    foreach ($_GET as $key => $value) {
                                      $get_filters_arr[] .= $key."=".$value;
                                    }
                                    $get_filters = "?".implode("&", $get_filters_arr);
                                  }
                                  ?>
                                  <a href="<?php echo $BASE_URL; ?>export-table.php<?php echo $get_filters; ?>" class="btn btn-success">Export</a>
                                </div>


                              </div>
                            </form>
                          </div>
                        </div>
                        <!-- Table Filters end -->
                        <br>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Seller Name</th>
                                    <th>Date Registered</th>
                                    <th>Contact No.</th>
                                    <th>Email</th>
                                    <th>Buying Purpose</th>
                                    <th style="width:250px">Locations Interested In</th>
                                    <th>Find out Amaia by</th>
                                    <th>Walk In</th>
                                    <!-- <th>Guests No.</th> -->
                                    <th>Age</th>
                                    <th>Gender</th>
                                    <th>Civil Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($registrations as $registration_id => $arr_registration) { 
                                        if(isset($arr_registration['showrooms'])){ ?>
                                  <tr>
                                    <td><?php echo $arr_registration['f_name']." ".$arr_registration['l_name']; ?></td>
                                    <td><?php echo $arr_registration['seller_name']; ?></td>
                                    <td><?php echo $arr_registration['date_registered']; ?></td>
                                    <td><?php echo $arr_registration['contact_num']; ?></td>
                                    <td><?php echo $arr_registration['e_mail']; ?></td>
                                    <td><?php echo $all_data_purpose[$arr_registration['buying_purpose']]; ?></td>
                                    <td style="width:250px"><?php 
                                        foreach ($arr_registration['showrooms'] as $arr_showrooms) {
                                          $exploded_showrooms = explode(",", $arr_showrooms['projects']);
                                          // print_r($exploded_showrooms);
                                          foreach ($exploded_showrooms as $clean_showroom) {
                                            $sanitized_showroom = str_replace("*", "", $clean_showroom);
                                            echo $all_data_showrooms[$sanitized_showroom]['title']."<br> ";  
                                          }                                         
                                        }
                                    ?></td>
                                    <td><?php echo $all_data_source[$arr_registration['find_out_amaia']]; ?></td>
                                    <td><?php echo $arr_registration['is_walk_in'] == 1 ? "Yes" : "No"; ?></td>
                                    <!-- <td><?php echo $arr_registration['guests_num']; ?></td> -->
                                    <td><?php echo $arr_registration['age']; ?></td>
                                    <td><?php echo $arr_registration['gender'] < 2 ? ($arr_registration['gender'] == 1 ? "Male" : "Female") : "Others"; ?></td>
                                    <td><?php echo $all_data_civil_status[$arr_registration['civil_status']]; ?></td>
                                  </tr>
                                  <?php } ?>
                                <?php } ?> 
                                </tbody>
                            </table>
                            <?php if(isset($total_pages)){ ?>
                            <?php
                                  $get_filters = "";
                                  if(isset($_GET))
                                  {
                                    $get_filters_arr = array();
                                    foreach ($_GET as $key => $value) {
                                      if($key != 'p')
                                      $get_filters_arr[] .= $key."=".$value;
                                    }
                                    $get_filters = implode("&", $get_filters_arr);
                                  }
                            ?>
                            <div class="pull-right col1 margtop20">
                              <div class="btn-group">
                                   <?php for($i=1; $i<=$total_pages; $i++) { ?>
                                  <a href="<?php echo $BASE_URL; ?>registrations.php<?php if($i!=1){echo '?'.$get_filters.'&p='.$i;} ?>"><button class="btn btn-success <?php if(($i == 1) && (!isset($_GET['p']))){ echo 'active'; }elseif(isset($_GET['p']) && $_GET['p'] == $i){ echo 'active'; } ?>" type="button"><?php echo $i; ?></button></a>
                              <?php } ?>
                              </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                  </section>
                </div>  
              </div>