  <?php 
  include("jp_library/jp_lib.php");
  include("head.php");
  include("w_services/dashboard_functions.php"); 
        $all_data_showrooms = getAllProjects(false, $data, true);
  ?>
  <body>
  <section id="container" class="">
      <?php include("header.php"); ?>
      <?php include("sidebar.php"); ?>
      <!--main content start-->
      <section id="main-content" >
          <section class="wrapper site-min-height">
              <!-- page start-->
              <!-- Graph start -->
              <div id="morris">
                  <div class="row">
                      <div class="col-lg-12">
                          <section class="panel">
                              <header class="panel-heading">
                                  No. of Registrations Interested in per Project 
                                  <?php if(!isset($_GET['start_date']) && !isset($_GET['end_date'])) { ?>
                                  <b>This Month</b>
                                  <?php } else { if($_GET['start_date'] == "" && $_GET['end_date'] == "") { ?>
                                  <b>This Month</b>
                                  <?php } }?>
                              </header>                             
                              <div class="panel-body" style="min-height:510px;display:block">
                              <!-- Table Filters start -->
                                <div class="row">
                                  <div class="col-lg-10">
                                    <form method="GET" name="reg_filters" id="reg_filters">
                                      <div class="form-group">
                                        <label class="control-label col-md-1">Date Range</label>
                                        <div class="col-md-2">
                                          <input class="form-control form-control-inline input-medium default-date-picker"  name="start_date" id="start_date" size="16" type="text" value="<?php echo isset($_GET['start_date']) ? $_GET['start_date'] : ""; ?>" />
                                          <span class="help-block">Start date</span>
                                        </div>
                                        <div class="col-md-2">
                                          <input class="form-control form-control-inline input-medium default-date-picker"  name="end_date" id="end_date" size="16" type="text" value="<?php echo isset($_GET['end_date']) ? $_GET['end_date'] : ""; ?>" />
                                          <span class="help-block">End date</span>
                                        </div>

                                        <label class="control-label col-md-1">Showroom</label>
                                        <div class="col-md-3">
                                            <select class="form-control" name="showroom">
                                              <option value="">Select showroom</option>
                                            <?php foreach ($all_data_showrooms as $showroom_id => $arr_value) { ?>
                                              <option value="<?php echo $showroom_id; ?>" <?php echo isset($_GET['showroom']) && $_GET['showroom'] == $showroom_id ? "selected" : ""; ?>><?php echo $arr_value['title']; ?></option>
                                            <?php } ?>
                                          </select>

                                        </div>
                                        <div class="col-md-3">
                                          <input type="hidden" name="p" id="p" value="<?php echo isset($_GET['p']) ? $_GET['p'] : ""; ?>">
                                          <button class="btn btn-success" type="submit">Search</button>
                                          <a href="<?php echo $BASE_URL.$PAGE_NAME; ?>" class="btn btn-success">Clear</a>
                                          <?php
                                          $get_filters = "";
                                          if(isset($_GET))
                                          {
                                            $get_filters_arr = array();
                                            foreach ($_GET as $key => $value) {
                                              $get_filters_arr[] .= $key."=".$value;
                                            }
                                            $get_filters = "?".implode("&", $get_filters_arr);
                                          }
                                          ?>
                                          <a href="<?php echo $BASE_URL; ?>export-interested.php<?php echo $get_filters; ?>" class="btn btn-success">Export</a>
                                        </div>


                                      </div>
                                    </form>
                                  </div>
                                </div>
                                <!-- Table Filters end -->
                                <div class="tab-pane" id="chartjs">
                                  <div class="row">
                                      <div class="col-lg-12">
                                        <section class="panel">
                                          <header class="panel-heading">
                                              Showroom Visits per Age Group
                                          </header>
                                          <div class="panel-body">
                                          <br>
                                            <div class='col-md-12'>
                                              <div class='row' style='margin-bottom:4px;'>
                                                <div class='col-lg-1 color-col' style='background:rgb(255,117,117);'>
                                                </div>
                                                <div class='col-lg-11'>
                                                <b>30 and below</b>
                                                </div>
                                              </div>
                                              <div class='row' style='margin-bottom:4px;'>
                                                <div class='col-lg-1 color-col' style='background:rgba(0,255,0,0.5);'>
                                                </div>
                                                <div class='col-lg-11'>
                                                <b>31 - 40</b>
                                                </div>
                                              </div>
                                              <div class='row' style='margin-bottom:4px;'>
                                                <div class='col-lg-1 color-col' style='background:rgba(0,0,255,0.5);'>
                                                </div>
                                                <div class='col-lg-11'>
                                                <b>41 - 50</b>
                                                </div>
                                              </div>
                                              <div class='row' style='margin-bottom:4px;'>
                                                <div class='col-lg-1 color-col' style='background:rgb(128,0,0);'>
                                                </div>
                                                <div class='col-lg-11'>
                                                <b>51 - 60</b>
                                                </div>
                                              </div>
                                              <div class='row' style='margin-bottom:4px;'>
                                                <div class='col-lg-1 color-col' style='background:rgba(255,255,0,0.5);'>
                                                </div>
                                                <div class='col-lg-11'>
                                                <b>61 - 70</b>
                                                </div>
                                              </div>
                                              <div class='row' style='margin-bottom:4px;'>
                                                <div class='col-lg-1 color-col' style='background:rgba(255,0,255,0.5);'>
                                                </div>
                                                <div class='col-lg-11'>
                                                <b>above 70</b>
                                                </div>
                                              </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="panel-body text-center">
                                              <canvas id="bar" height="750" width="1500"></canvas>
                                          </div>
                                        </section>
                                      </div>
                                  </div>
                                </div>
                              </div>
                          </section>
                      </div>
                  </div>
              </div>
              <!-- Graph end -->
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <?php include("footer.php"); ?>
  </section>
    <?php include("scripts.php"); ?>
    <script> 
        
    </script>
  </body>
</html>
 <!-- script for this page only-->
  <script src="assets/chart-master/Chart.js"></script>
<script>
 $.ajax({
    url:"w_services/get_db_data_age.php",
    type: "POST",
    dataType: "json",
    data: $('#reg_filters').serialize(),
    success:function(data){
      console.log(data);
      var labels_data=[];
      var data_1=[];  //30 and below
      var data_2=[];  //31 - 40
      var data_3=[];  //41 - 50
      var data_4=[];  //51 - 60
      var data_5=[];  //61 - 70
      var data_6=[];  //above 70

      for(var n in data){
        labels_data.push(data[n]["title"]);
        data_1.push(data[n]["data_1"]);
        data_2.push(data[n]["data_2"]);
        data_3.push(data[n]["data_3"]);
        data_4.push(data[n]["data_4"]);
        data_5.push(data[n]["data_5"]);
        data_6.push(data[n]["data_6"]);
      }
      var barChartData = {
            labels : labels_data,
            datasets : [
            {
                fillColor : "rgb(255,117,117)",
                strokeColor : "rgba(151,187,205,1)",
                data : data_1
            },
            {
                fillColor : "rgba(0,255,0,0.5)",
                strokeColor : "rgba(151,187,205,1)",
                data : data_2
            },
            {
                fillColor : "rgba(0,0,255,0.5)",
                strokeColor : "rgba(151,187,205,1)",
                data : data_3
            },
            {
                fillColor : "rgb(128,0,0)",
                strokeColor : "rgba(151,187,205,1)",
                data : data_4
            },
            {
                fillColor : "rgba(255,255,0,0.5)",
                strokeColor : "rgba(151,187,205,1)",
                data : data_5
            },
            {
                fillColor : "rgba(255,0,255,0.5)",
                strokeColor : "rgba(151,187,205,1)",
                data : data_6
            }
          ]

      };
      new Chart(document.getElementById("bar").getContext("2d")).Bar(barChartData);
    },
    error:function(e)
    {
      console.log(e);
    }
  });   
</script>

