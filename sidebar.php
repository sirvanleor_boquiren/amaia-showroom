    <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
                  <li>
                      <a href="dashboard.php" <?php echo $PAGE_NAME == "dashboard.php" ? "class='active'" : ""; ?>>
                          <i class="fa fa-bar-chart-o"></i>
                          <span>Showroom Visits</span>
                      </a>
                  </li>
                  <li>
                      <a href="projects_interested.php" <?php echo $PAGE_NAME == "projects_interested.php" ? "class='active'" : ""; ?>>
                          <i class="fa fa-bar-chart-o"></i>
                          <span>Projects Interested In</span>
                      </a>
                  </li>
                  <li>
                      <a href="registrations.php" <?php echo $PAGE_NAME == "registrations.php" ? "class='active'" : ""; ?>>
                          <i class="fa fa-users"></i>
                          <span>Registrations</span>
                      </a>
                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->