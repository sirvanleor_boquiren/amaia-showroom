<?php

namespace Amaia_showroom\Helpers;



/**
* This is a helper class for importing/exporting CSV files
* @author: @jclxz
*/



class CSV {



  /**
  * holds all the column names for the CSV
  * @var array
  */

  public $columns;



  /**
  * all rows to be exported
  * @var array
  */

  public $data;



  /**
  * can set default set of columns upon initialization
  * @param array $columns
  */

  public function __construct($columns = null)

  {

    $this->columns = $columns;

  }



  /**
  * adds a new row in the CSV
  * @param array $data
  */

  public function addRow($data)

  {

    $this->data[] = $data;

  }



  /**
  * exports a csv file from $columns and $data
  * @param  string $filename filename for the CSV including the .csv extension
  * @return void
  */

  public function export($filename = "Showroom_report")

  {

    $filename = $filename."_".date("Y_m_d").".csv";



    header('Content-Type: text/csv; charset=utf-8');

    header('Content-Disposition: attachment; filename=' . $filename);



    $output = fopen('php://output', 'w');



    fputcsv($output, $this->columns);



    foreach ($this->data as $data) {

      fputcsv($output, $data);

    }



  }



  public function downloadVolunteerTemplate()

  {

    $filename = "downloaded.csv";

    header('Content-Type: text/csv; charset=utf-16le');

    header('Content-Disposition: attachment; filename=' . $filename);

    $output = fopen('php://output', 'w');

    fputcsv($output, $this->columns);

  }

}

