  <?php include("jp_library/jp_lib.php"); ?>
  <?php include("head.php"); ?>
  <?php include("w_services/dashboard_functions.php"); ?>
  <?php 
  

  if(isset($_GET['p']))
  {
    $page = 1;
    if(isset($_GET['p']))
    {
      $page = $_GET['p'];
    }
    if(isset($_GET['start_date']))
    {
      $registrations = getRegistrations(false, $page, $_GET['start_date'], $_GET['end_date'], $_GET['showroom']);
      $total_pages = getRegistrations(false, $page, $_GET['start_date'], $_GET['end_date'], $_GET['showroom'], true);
    }
    else
    {
      $registrations = getRegistrations(false, $page);
      $total_pages = getTotalPages();
    }
  }
  else
  {
    $registrations = getRegistrations();
    $total_pages = getTotalPages();
  }


  
  // $all_data_showrooms = getAllShowrooms(false, $data);
  $all_data_showrooms = getAllProjects(false, $data);
  $all_data_showrooms_select = getAllProjects(false, $data, true);
  $all_data_source = getAllData(false, $data, 'source');
  $all_data_purpose = getAllData(false, $data, 'purpose');
  $all_data_age = getAllData(false, $data, 'age');
  $all_data_civil_status = getAllData(false, $data, 'civil_status');

  // print_r($total_pages);
  ?>

  <body>
  <section id="container" class="">
      <?php include("header.php"); ?>
      <?php include("sidebar.php"); ?>
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <!-- Reponsive Table start -->
              <?php include("registrations_table.php"); ?>
              <!-- Responsive Table end -->
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <?php include("footer.php"); ?>
  </section>
    <?php include("scripts.php"); ?>
  </body>
</html>
