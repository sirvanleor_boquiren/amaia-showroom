<?php 
include("jp_library/jp_lib.php"); 
include("w_services/dashboard_functions.php");
if(isset($_GET['p']))
{
	$page = 1;
	if(isset($_GET['p']))
	{
	  $page = $_GET['p'];
	}
	if(isset($_GET['start_date']))
	{
	  $registrations = getRegistrations(false, $page, $_GET['start_date'], $_GET['end_date'], $_GET['showroom']);
	  $total_pages = getRegistrations(false, $page, $_GET['start_date'], $_GET['end_date'], $_GET['showroom'], true);
	}
	else
	{
	  $registrations = getRegistrations(false, $page);
	  $total_pages = getTotalPages();
	}
}
else
{
	$registrations = getRegistrations();
	$total_pages = getTotalPages();
}



$all_data_showrooms = getAllShowrooms(false, $data);
$all_data_source = getAllData(false, $data, 'source');
$all_data_purpose = getAllData(false, $data, 'purpose');
$all_data_age = getAllData(false, $data, 'age');
$all_data_civil_status = getAllData(false, $data, 'civil_status');


$columns = [
	"Name",
	"Seller Name",
	"Date Registered",
	"Contact No.",
	"Email",
	"Buying Purpose",
	"Locations Interested In",
	"Find out Amaia by",
	"Walk in",
	"Guests No.",
	"Age",
	"Gender",
	"Civil Status"
];
$csv = new CSV($columns);

?>
