<?php
$include="dashboard";
include('get_data.php');
// $data contains data from json file
date_default_timezone_set("Asia/Manila");

if (isset($_GET['action']) && $_GET['action'] != '') {
  require("../jp_library/jp_lib.php");

  if(isset($_POST['data']))
  {
    parse_str($_POST['data'], $_POST);
    $_POST['data'] = false;
  }
  $_GET['action'](true);
  if (!isset($_SESSION['user_id'])) {
    session_start();
  }
}


function getRegistrations($ajax = false, $page = "", $start_date = "", $end_date = "", $showroom = "", $get_count = false, $get_export = false)
{
	$get_registrations['table'] = "registration";
	$get_registrations['where'] = 1;
	// $get_registrations['debug'] = 1;

	// filters start
	if($start_date != "")
	{
		$start_date = date("Y-m-d H:i:s", strtotime($start_date));
		$get_registrations['where'] .= " AND date_registered >= '".$start_date."'";
	}
	if($end_date != "") 
	{
		$end_date = date("Y-m-d H:i:s", strtotime($end_date));
		$get_registrations['where'] .= " AND date_registered <= '".$end_date."'";
	}
	// filters end

	
	if($get_count == false && $get_export == false && $showroom == "")
	{
		$filter = " LIMIT 0, 10";
		if($page != "")
		{
			$limit_offset = 10 * ($page - 1);
	      	$filter = " LIMIT ".$limit_offset.", 10";
		}
		$get_registrations['filters'] = "ORDER BY registration_id DESC".$filter;		
	}

	$res_registrations = jp_get($get_registrations);
	$data = array();
	while($row_registrations = mysqli_fetch_assoc($res_registrations))
	{
		$data[$row_registrations['registration_id']] = $row_registrations;
		$get_showrooms['table'] = "location_interested";
		$get_showrooms['where'] = "registration_id = ".$row_registrations['registration_id'];
		// $get_showrooms['debug'] = 1;
		if($showroom != "")
		{
			$get_showrooms['where'] .= " AND properties LIKE '%".$showroom."%'";
		}
		$res_showrooms = jp_get($get_showrooms);
		while($row_showrooms = mysqli_fetch_assoc($res_showrooms))
		{
			$data[$row_registrations['registration_id']]['showrooms'][] = $row_showrooms;
		}
	}

	if($get_count != false)
	{
		$count_data = 0;
		foreach ($data as $key => $value) {
			if(isset($value['showrooms']))
			{
				$count_data++;
			}
		}
		$ctr = 10;
		if($count_data > 10)
		{
			while($ctr < $count_data)
			{
				$ctr += 10;
			}
		}
		$total_pages = $ctr / 10;
		$count_data = $total_pages;
		return $count_data;
	}
	else
	{
		return $data;
	}
	// print_r($count_data);
}

function getTotalPages($ajax = false, $page = "", $start_date = "", $end_date = "", $showroom = "")
{
	$get_all_registrations['select'] = "COUNT(registration_id) as total";
	$get_all_registrations['table'] = "registration";
	$get_all_registrations['where'] = "1";
	$res_all_registration = jp_get($get_all_registrations);
	$row_all_registration = mysqli_fetch_assoc($res_all_registration);
	$all_registration = $row_all_registration['total'];
	$ctr = 10;
	if($all_registration > 10)
	{
		while($ctr < $all_registration){
			$ctr += 10;
		}
	}
	$total_pages = $ctr / 10;
	return $total_pages;
}

function getAllShowrooms($ajax = false, $data)
{
	$showrooms = array();
	foreach ($data['locations'] as $loc_key => $loc_value) {
		foreach ($loc_value['properties'] as $pro_key => $pro_value) {
			// $prop_id[] = $pro_value["id"];
			$showrooms[$pro_value['id']]['title'] = $pro_value["title"];
		}	
	}
	return $showrooms;
}

function getAllProjects($ajax = false, $data, $projects_interested = false)
{
	$projects = array();
	// var_dump($data);
	foreach ($data['locations'] as $loc_key => $loc_value) {
		foreach ($loc_value['properties'] as $pro_value) {
			foreach ($pro_value['projects'] as $project_arr) {
				if($projects_interested == true)
				{
					$projects[$project_arr['project_reports_id']]['title'] = $project_arr["title"];	
				}
				else
				{
					$projects[$project_arr['id']]['title'] = $project_arr["title"];	
				}
			}
		}	
	}
	return $projects;
}


function getAllData($ajax = false, $data, $json_id)
{
	$all_data = array();
	foreach ($data[$json_id] as $arr_data) {
		$all_data[$arr_data['id']] = $arr_data["title"];	
	}
	return $all_data;
}

	// $final_data = array();
	// foreach ($data['locations'] as $loc_key => $loc_value) {
	// 	foreach ($loc_value['properties'] as $pro_key => $pro_value) {
	// 		$prop_id[] = $pro_value["id"];
	// 		$final_data[$pro_value['id']]['title'] = $pro_value["title"];
	// 	}	
	// }

	// foreach ($prop_id as $key => $value) {
	// 	$count_data['select'] = "COUNT(location_id) as total";
	// 	$count_data['table'] = "location_interested";
	// 	$count_data['where'] = "properties LIKE '%*".$value."*%'";
	// 	$res_data = jp_get($count_data);
	// 	$row_data = mysqli_fetch_assoc($res_data);
	// 	$final_data[$value]['amount'] = $row_data['total'];
	// }
	// echo json_encode($final_data);


?>