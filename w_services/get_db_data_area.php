<?php
include('../jp_library/jp_lib.php');
$include="function";
include('get_data.php');
// $data contains data from json file
date_default_timezone_set("Asia/Manila");
if($_POST)
{
	$final_data = array();
	$final_data['error_msg'] = "";
	foreach ($data['locations'] as $loc_key => $loc_value) {
		foreach ($loc_value['properties'] as $pro_key => $pro_value) {
			if(isset($_POST['showroom']) && $_POST['showroom'] != '')
			{
				if($pro_value["id"] == $_POST['showroom'])
				{
					$prop_id[] = $pro_value["id"];
					$final_data["color"][] = $pro_value["color_code"];
					$final_data["labels"][] = $pro_value["title"];
					$final_data["ykeys"][] = $pro_value["id"];
				}
			}
			if(isset($_POST['showroom']) && $_POST['showroom'] != '')
			{
				if($pro_value["id"] == $_POST['showroom'])
				{
					$prop_id[] = $pro_value["id"];
					$final_data["color"][] = $pro_value["color_code"];
					$final_data["labels"][] = $pro_value["title"];
					$final_data["ykeys"][] = $pro_value["id"];
				}
			}
			else
			{
				$prop_id[] = $pro_value["id"];
				$final_data["color"][] = $pro_value["color_code"];
				$final_data["labels"][] = $pro_value["title"];
				$final_data["ykeys"][] = $pro_value["id"];	
			}
		}	
	}
	if(isset($_POST['type']) && $_POST['type'] == "m")
	{
		$start_year = date("Y");		# DEFAULT: Year of today
		$start_month = "01";			# DEFAULT: January first month
		$end_year = date("Y");			# DEFAULT: Year Today
		$end_month = date("m");			# DEFAULT: Month today

		$months_diff = "-12"; 			# DEFAULT: Date difference is from month today and last 12 months
		$changed = 0;


		if(isset($_POST['start_month']) && $_POST['start_month'] != '')
		{
			$start_month = $_POST['start_month'];
			if($start_month < 10)
			{
				$start_month = "0".$start_month;
			}
			$changed = 1;
		}

		if(isset($_POST['end_month']) && $_POST['end_month'] != '')
		{
			$end_month = $_POST['end_month'];
			if($end_month < 10)
			{
				$end_month = "0".$end_month;
			}
		}

		if(isset($_POST['start_year']) && $_POST['start_year'] !='')
		{
			$start_year = $_POST["start_year"];
			$changed = 1; 			# Start Year Value Set
		}
		
		if(isset($_POST['end_year']) && $_POST['end_year'] !='')
		{
			$end_year = $_POST['end_year'];
		}

		$end_date = $end_year."-".$end_month."-".date("d");
		if($changed == 0)
		{
			$start_date = date("Y-m-d", strtotime("-12 months", strtotime($end_date)));
			$start_month = date("m", strtotime($start_date));
			$start_year = date("Y", strtotime($start_date));
		}
		else
		{
			$start_date = $start_year."-".$start_month."-".date("d");
		}

		$final_data['error_msg'] = "End Date should be GREATER than Start Date";
		if($end_year >= $start_year)
		{
			$final_data['error_msg'] = "";
			if($end_month >= $start_month)
			{
				$final_data['error_msg'] = "";
				$date_first = new DateTime($end_date);
				$date_second = new DateTime($start_date);
				$date_diff = $date_second->diff($date_first);
				$years = $date_diff->format("%y");
				$months_diff = $date_diff->format("%m");
				if($years > 0)
				{
					$months_diff += $years * 12;
				}
				// echo $months_diff;
				$ctr_date = $start_date;
				for($ctr=0;$ctr<=$months_diff;$ctr++) {
					$final_data['graph_plot'][$ctr_date] = array();
					$ctr_month = date("m", strtotime($ctr_date));
					$ctr_year = date("Y", strtotime($ctr_date));
					foreach ($prop_id as $key => $value) {
						$count_data['select'] = "COUNT(location_id) as total";
						$count_data['table'] = "location_interested";
						$count_data['where'] = "properties = '$value' AND extract(month from date_registered) = $ctr_month AND extract(year from date_registered) = $ctr_year";
						$res_data = jp_get($count_data);
						$row_data = mysqli_fetch_assoc($res_data);
						$final_data['graph_plot'][$ctr_date][$value] = $row_data['total'];

					}
					$final_data['graph_plot'][$ctr_date]['period'] = $ctr_date;
					$ctr_date = date("Y-m-d", strtotime("+1 month", strtotime($ctr_date)));
				}
			}
		}
	}
	else
	{
		$month = date("m", strtotime(date("Y-m-d")));
		$year = date("Y", strtotime(date("Y-m-d")));
		
		if(isset($_POST['year']) && $_POST['year'] != '')
		{
			$year = $_POST['year'];
		}
		if(isset($_POST['month']) && $_POST['month'] != '')
		{
			$month = $_POST['month'];
		}
		$date = $year."-".$month."-01";
		$final_date = date("t", strtotime($date));
		$final_data['graph_plot'] = array();
		for ($start_day=1; $start_day < $final_date; $start_day++) { 
			$final_data['graph_plot'][$date] = array();
			// $ctr_date = $date.$start_day;
			foreach ($prop_id as $key => $value) {
				$count_data['select'] = "COUNT(location_id) as total";
				$count_data['table'] = "location_interested";
				$count_data['where'] = "properties = '$value' AND date_registered = '$date'";
				// $count_data['debug'] = "1";
				$res_data = jp_get($count_data);
				$row_data = mysqli_fetch_assoc($res_data);
				$final_data['graph_plot'][$date][$value] = $row_data['total'];
			}
			$final_data['graph_plot'][$date]['period'] = $date;
			$date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
		}	
	}


	echo json_encode($final_data);
}


?>