<?php
include('../jp_library/jp_lib.php');
$include=true;
include('get_data.php');
// $data contains data from json file

function send_email($to,$name)
{
	$message = "Hi ".$name."! \n \n";
	$message .= "Thank you for visiting and submitting your feedback with us. \n";
	$message .= "We greatly appreciate it. \n";
	$message .= "Thank you!";
	$headers = "From:svboquiren@myoptimind.com \r\n";
	$subject = "Amaia Showroom";
	mail($to,$subject,$message,$headers);
}

if(isset($_POST))
{
	$return = array();       														# return for Webservice Response
	$return['status'] = 400; 														# default error
	if(is_array($_POST['arr_data']))
	{
		foreach ($_POST['arr_data'] as $post_data) {
			// print_r($post_data);
			$add_registration['table'] = "registration";
			$add_registration['data'] = $post_data;
			if(jp_add($add_registration))
			{
				send_email($post_data['e_mail'], $post_data['f_name']." ".$post_data['l_name']);
				$user_id = jp_last_added();													# to be used for table: location_interested
			}
			// $locations_data = array();
			if(isset($post_data['properties']) && $post_data['properties'] != '')
			{
				if(isset($post_data['projects']) && $post_data['projects'])
				{
					$projects = array();
					$temp_projects = explode(",", $post_data['projects']);
					foreach ($temp_projects as $temp_project) {
						$projects[] .= "*".$temp_project."*";
					}
					$string_projects = implode(",", $projects);
				}
			}
			$clean_data = array(); 			 												# instantiate (and clear) arrays to be used
			$locations_clean_data = array(); 												# instantiate (and clear) arrays to be used

			$locations_clean_data['registration_id'] = $user_id;							
			$get_location = explode("-", $post_data['properties']);
			$locations_clean_data['location'] = $get_location[0];
			$locations_clean_data['properties'] = $post_data['properties'];
			$locations_clean_data['projects'] = $string_projects;
			$locations_clean_data['date_registered'] = $post_data['date_registered'];

			$add_locations['table'] = "location_interested";								
			$add_locations['data'] = $locations_clean_data;

			if(!jp_add($add_locations))
			{
				$return['status'] = 400;
				echo json_encode($return);
			}
			else
			{
				$return['status'] = 200;
			}

			# This is for array of properties - start
			// foreach ($locations_data as $location => $arr_properties) {
			// 	$locations_clean_data['location'] = $location;
			// 	$locations_clean_data['properties'] = implode(",", $arr_properties['properties']);
			// 	$add_locations['data'] = $locations_clean_data;
			// 	if(!jp_add($add_locations))
			// 	{
			// 		$return['status'] = 400;
			// 		echo json_encode($return);
			// 	}
			// 	else
			// 	{
			// 		$return['status'] = 200;
			// 	}
			// }
			# This is for array of properties - end	
		}
	}
	echo json_encode($return);
}
?>