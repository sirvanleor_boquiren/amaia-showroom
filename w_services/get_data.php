<?php

date_default_timezone_set("Asia/Manila");
$file_location = "../amaia_data/__data.json";
if(isset($include))
{
	if($include != "function")
	{
		$file_location = "amaia_data/__data.json";
	}
}
$json_file = file_get_contents($file_location);
$data = json_decode($json_file, true);
$data['date_modified'] = date("Y-m-d H:i:s", filemtime($file_location));
if(!isset($include))
{
	echo json_encode($data);	
}


?>