<?php
include('../jp_library/jp_lib.php');
$include="function";
include('get_data.php');
// $data contains data from json file
date_default_timezone_set("Asia/Manila");
if($_POST)
{

	$final_data = array();
	if(isset($_POST['showroom']) && $_POST['showroom'] != "")
	{
		foreach ($data['locations'] as $loc_key => $loc_value) {
			foreach ($loc_value['properties'] as $pro_key => $pro_value) {
				foreach ($pro_value['projects'] as $projects_arr) {	
					if($projects_arr['project_reports_id'] == $_POST['showroom'])
					{
						$prop_id[] = $projects_arr;
						$final_data[$projects_arr['project_reports_id']]['title'] = $projects_arr["title"];
						$final_data[$projects_arr['project_reports_id']]['data_1'] = 0;
						$final_data[$projects_arr['project_reports_id']]['data_2'] = 0;
						$final_data[$projects_arr['project_reports_id']]['data_3'] = 0;
						$final_data[$projects_arr['project_reports_id']]['data_4'] = 0;
						$final_data[$projects_arr['project_reports_id']]['data_5'] = 0;
						$final_data[$projects_arr['project_reports_id']]['data_6'] = 0;
					}
				}
			}
		}
	}
	else
	{
		foreach ($data['locations'] as $loc_key => $loc_value) {
			foreach ($loc_value['properties'] as $pro_key => $pro_value) {
				foreach ($pro_value['projects'] as $projects_arr) {
					$prop_id[] = $projects_arr;
					$final_data[$projects_arr['project_reports_id']]['title'] = $projects_arr["title"];
					$final_data[$projects_arr['project_reports_id']]['data_1'] = 0;
					$final_data[$projects_arr['project_reports_id']]['data_2'] = 0;
					$final_data[$projects_arr['project_reports_id']]['data_3'] = 0;
					$final_data[$projects_arr['project_reports_id']]['data_4'] = 0;
					$final_data[$projects_arr['project_reports_id']]['data_5'] = 0;
					$final_data[$projects_arr['project_reports_id']]['data_6'] = 0;
				}
			}	
		}
	}

	$where_ext = " AND li.date_registered LIKE '%".date("Y")."-".date("m")."%'";
	if(isset($_POST['start_date']) || isset($_POST['end_date']))
	{
		if($_POST['start_date'] != "" || $_POST['end_date'] != "")
		{
			$start_date = $end_date = date("Y-m-d H:i:s", strtotime(date("Y-m-d 23:59:59")));
			if($_POST['start_date'] != "")
			{
				$start_date = date("Y-m-d H:i:s", strtotime($_POST['start_date']." 23:59:59"));
			}
			if($_POST['end_date'] != "")
			{
				$end_date = date("Y-m-d H:i:s", strtotime($_POST['end_date']." 23:59:59"));
			}
			$where_ext = " AND (li.date_registered between '$start_date' AND '$end_date')";	
		}
	}

	foreach ($prop_id as $projects_arr) {
		$count_data['select'] = "COUNT(location_id) as total";
		$count_data['table'] = "location_interested li LEFT JOIN registration r ON li.registration_id = r.registration_id";
		// $count_data['debug'] = 1;
		$const_where = "li.projects LIKE '%*".$projects_arr['id']."*%'".$where_ext;
		# Under 30
		$count_data['where'] = $const_where." AND r.age < 31";
		$res_data = jp_get($count_data);
		$row_data = mysqli_fetch_assoc($res_data);
		$final_data[$projects_arr['project_reports_id']]['data_1'] += $row_data['total'];
		# 31 - 40
		$count_data['where'] = $const_where." AND (r.age > 30 AND r.age < 41)";
		$res_data = jp_get($count_data);
		$row_data = mysqli_fetch_assoc($res_data);
		$final_data[$projects_arr['project_reports_id']]['data_2'] += $row_data['total'];
		# 41 - 50
		$count_data['where'] = $const_where." AND (r.age > 40 AND r.age < 51)";
		$res_data = jp_get($count_data);
		$row_data = mysqli_fetch_assoc($res_data);
		$final_data[$projects_arr['project_reports_id']]['data_3'] += $row_data['total'];
		# 51 - 60
		$count_data['where'] = $const_where." AND (r.age > 50 AND r.age < 61)";
		$res_data = jp_get($count_data);
		$row_data = mysqli_fetch_assoc($res_data);
		$final_data[$projects_arr['project_reports_id']]['data_4'] += $row_data['total'];
		# 61 - 70
		$count_data['where'] = $const_where." AND (r.age > 60 AND r.age < 71)";
		$res_data = jp_get($count_data);
		$row_data = mysqli_fetch_assoc($res_data);
		$final_data[$projects_arr['project_reports_id']]['data_5'] += $row_data['total'];
		# above 70
		$count_data['where'] = $const_where." AND (r.age > 60 AND r.age < 71)";
		$res_data = jp_get($count_data);
		$row_data = mysqli_fetch_assoc($res_data);
		$final_data[$projects_arr['project_reports_id']]['data_6'] += $row_data['total'];
	}
	echo json_encode($final_data);
}


?>