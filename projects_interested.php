  <?php 
  include("jp_library/jp_lib.php");
  include("head.php");
  include("w_services/dashboard_functions.php"); 
        $all_data_showrooms = getAllProjects(false, $data, true);
  ?>
  <body>
  <section id="container" class="">
      <?php include("header.php"); ?>
      <?php include("sidebar.php"); ?>
      <!--main content start-->
      <section id="main-content" >
          <section class="wrapper site-min-height">
              <!-- page start-->
              <!-- Graph start -->
              <div id="morris">
                  <div class="row">
                      <div class="col-lg-12">
                          <section class="panel">
                              <header class="panel-heading">
                                  No. of Registrations Interested in per Project 
                                  <?php if(!isset($_GET['start_date']) && !isset($_GET['end_date'])) { ?>
                                  <b>This Month</b>
                                  <?php } else { if($_GET['start_date'] == "" && $_GET['end_date'] == "") { ?>
                                  <b>This Month</b>
                                  <?php } }?>
                              </header>                             
                              <div class="panel-body" style="min-height:510px;display:block">
                              <!-- Table Filters start -->
                                <div class="row">
                                  <div class="col-lg-10">
                                    <form method="GET" name="reg_filters" id="reg_filters">
                                      <div class="form-group">
                                        <label class="control-label col-md-1">Date Range</label>
                                        <div class="col-md-2">
                                          <input class="form-control form-control-inline input-medium default-date-picker"  name="start_date" id="start_date" size="16" type="text" value="<?php echo isset($_GET['start_date']) ? $_GET['start_date'] : ""; ?>" />
                                          <span class="help-block">Start date</span>
                                        </div>
                                        <div class="col-md-2">
                                          <input class="form-control form-control-inline input-medium default-date-picker"  name="end_date" id="end_date" size="16" type="text" value="<?php echo isset($_GET['end_date']) ? $_GET['end_date'] : ""; ?>" />
                                          <span class="help-block">End date</span>
                                        </div>

                                        <label class="control-label col-md-1">Showroom</label>
                                        <div class="col-md-3">
                                            <select class="form-control" name="showroom">
                                              <option value="">Select showroom</option>
                                            <?php foreach ($all_data_showrooms as $showroom_id => $arr_value) { ?>
                                              <option value="<?php echo $showroom_id; ?>" <?php echo isset($_GET['showroom']) && $_GET['showroom'] == $showroom_id ? "selected" : ""; ?>><?php echo $arr_value['title']; ?></option>
                                            <?php } ?>
                                          </select>

                                        </div>
                                        <div class="col-md-3">
                                          <input type="hidden" name="p" id="p" value="<?php echo isset($_GET['p']) ? $_GET['p'] : ""; ?>">
                                          <button class="btn btn-success" type="submit">Search</button>
                                          <a href="<?php echo $BASE_URL.$PAGE_NAME; ?>" class="btn btn-success">Clear</a>
                                          <?php
                                          $get_filters = "";
                                          if(isset($_GET))
                                          {
                                            $get_filters_arr = array();
                                            foreach ($_GET as $key => $value) {
                                              $get_filters_arr[] .= $key."=".$value;
                                            }
                                            $get_filters = "?".implode("&", $get_filters_arr);
                                          }
                                          ?>
                                          <a href="<?php echo $BASE_URL; ?>export-interested.php<?php echo $get_filters; ?>" class="btn btn-success">Export</a>
                                        </div>


                                      </div>
                                    </form>
                                  </div>
                                </div>
                                <!-- Table Filters end -->
                                <div id="hero-bar" class="graph">
                                  <!-- JS content: bottom of dashboard.php + script included( js/morris-script.js ) -->

                                </div> 
                              </div>
                          </section>
                      </div>
                  </div>
              </div>
              <!-- Graph end -->
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <?php include("footer.php"); ?>
  </section>
    <?php include("scripts.php"); ?>
    <script> 
      $.ajax({
        url:"w_services/get_db_data.php",
        type: "POST",
        dataType: "json",
        data: $('#reg_filters').serialize(),
        success:function(data){
          console.log(data);
          var graph_data=[];
          for(var n in data){
            graph_data.push({device : data[n]["title"], geekbench : data[n]['amount']});
          }
          Morris.Bar({
            element: 'hero-bar',
            data: graph_data,
            xkey: 'device',
            ykeys: ['geekbench'],
            labels: ['No. Of Visits'],
            barRatio: 0.4,
            xLabelAngle: 63,
            hideHover: 'auto',
            barColors: ['#51b848']
          });
          $("svg").attr('style', 'overflow:hidden; position:relative; min-height:450px; top:-0.234375px;');
        },
        error:function(e)
        {
          console.log(e);
        }
      });      
    </script>
  </body>
</html>
