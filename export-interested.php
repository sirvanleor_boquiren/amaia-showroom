<?php
namespace Amaia_showroom\Helpers;
include('jp_library/jp_lib.php');
include("w_services/dashboard_functions.php"); # $data contains all json
// $data contains data from json file
date_default_timezone_set("Asia/Manila");

$final_data = array();
$final_data['row_data'] = array();
$final_data['columns'][0] = "Date Range [start date - end date]";

$where_ext = " AND date_registered LIKE '%".date("Y")."-".date("m")."%'";
$final_data['row_data'][0] = "Month Today";
if(isset($_GET['start_date']) || isset($_GET['end_date']))
{
	if($_GET['start_date'] != "" || $_GET['end_date'] != "")
	{
		$start_date = $end_date = date("Y-m-d H:i:s", strtotime(date("Y-m-d 23:59:59")));
		if($_GET['start_date'] != "")
		{
			$start_date = date("Y-m-d H:i:s", strtotime($_GET['start_date']." 23:59:59"));
		}
		if($_GET['end_date'] != "")
		{
			$end_date = date("Y-m-d H:i:s", strtotime($_GET['end_date']." 23:59:59"));
		}
		$where_ext .= " AND (date_registered between '$start_date' AND '$end_date')";	
		$final_data['row_data'][0] = $start_date." - ".$end_date;
	}
}


if(isset($_GET['showroom']) && $_GET['showroom'] != "")
{
	foreach ($data['locations'] as $loc_key => $loc_value) {
		foreach ($loc_value['properties'] as $pro_key => $pro_value) {
			foreach ($pro_value['projects'] as $projects_arr) {	
				if($projects_arr['project_reports_id'] == $_GET['showroom'])
				{
					$prop_id[] = $projects_arr;
					$final_data['columns'][$projects_arr['project_reports_id']] = $projects_arr["title"];
					$final_data[$projects_arr['project_reports_id']]['amount'] = 0;
				}
			}
		}
	}
}
else
{
	foreach ($data['locations'] as $loc_key => $loc_value) {
		foreach ($loc_value['properties'] as $pro_key => $pro_value) {
			foreach ($pro_value['projects'] as $projects_arr) {
				$prop_id[] = $projects_arr;
				$final_data['columns'][$projects_arr['project_reports_id']] = $projects_arr["title"];
				$final_data['row_data'][$projects_arr['project_reports_id']] = 0;
			}
		}	
	}
}

foreach ($prop_id as $projects_arr) {
	$count_data['select'] = "COUNT(location_id) as total";
	$count_data['table'] = "location_interested";
	$count_data['where'] = "projects LIKE '%".$projects_arr['id']."%'".$where_ext;
	// $count_data['debug'] = 1;
	$res_data = jp_get($count_data);
	$row_data = mysqli_fetch_assoc($res_data);
	$final_data['row_data'][$projects_arr['project_reports_id']] += $row_data['total'];
}

// echo json_encode($final_data);
// var_dump($final_data);

$csv_columns = $final_data['columns'];
$csv = new CSV($csv_columns);
$csv->addRow($final_data['row_data']);
// foreach ($final_data['row_data'] as $date_of_visit => $csv_row) {
// }
$csv->export("Projects_Interested_In_Report");


?>